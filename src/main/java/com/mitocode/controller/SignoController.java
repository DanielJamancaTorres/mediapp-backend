package com.mitocode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Paciente;
import com.mitocode.model.Signo;
import com.mitocode.service.impl.SignoServiceImpl;

@RestController
@RequestMapping("/signos")
public class SignoController {
	
	@Autowired
	private SignoServiceImpl service;
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping(produces = "application/json")
	public List<Signo> listar(){
		return service.listar();
	}
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping(value = "/{id}", produces = "application/json")
	public Signo listarPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@GetMapping(value="/pageable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Signo>> listarPageable(Pageable pageable){
		Page<Signo> signos;
		signos = service.listarPageable(pageable);
		return new ResponseEntity<Page<Signo>>(signos, HttpStatus.OK);
	}
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public Signo registrar(@RequestBody Signo signo) {
		return service.registrar(signo);
	}
	
	@PutMapping(produces = "application/json", consumes = "application/json")
	public Signo modificar(@RequestBody Signo signo) {
		return service.modificar(signo);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
	
	

}
